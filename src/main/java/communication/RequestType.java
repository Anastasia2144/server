package communication;

public enum RequestType {
    CREATE,
    FIND,
    FIND_MASK,
    UPDATE,
    DELETE,
    EXIT;
}
