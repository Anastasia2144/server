package communication;

import java.io.Closeable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ConnectionClient implements Closeable {
    private final Socket socket;
    private final ObjectOutputStream out;
    private final ObjectInputStream in;

    public ConnectionClient(Socket socket) throws IOException {
        this.socket = socket;
        this.out = new ObjectOutputStream(socket.getOutputStream());
        this.in = new ObjectInputStream(socket.getInputStream());

    }

    public void send(String stringResponse) throws IOException {
        out.writeObject(stringResponse);
    }

    public String receive() throws IOException, ClassNotFoundException {
        return in.readObject().toString();
    }

    public void close() throws IOException {
        socket.close();
        out.close();
        in.close();
    }
}
