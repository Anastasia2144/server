package db;

import communication.Request;
import communication.Response;
import communication.ResponseType;

import java.sql.Connection;


public class DBManager {

    public Response doRequest(Connection connection, Request request){
        switch (request.getType()){
            case CREATE: {
                return DBRequest.create(connection, request.getWord(), request.getMeaning());
            }
            case FIND: {
                return DBRequest.find(connection, request.getWord());
            }
            case UPDATE:{
                return DBRequest.update(connection, request.getWord(), request.getMeaning());
            }
            case DELETE:{
                return DBRequest.delete(connection,request.getWord());
            }
            case FIND_MASK:{
                return DBRequest.findMask(connection, request.getWord());
            }
            //Вообще это уже обрабатывается в Клиенте. Запрос не может прийти неверный.
            default:{
                Response response = new Response();
                response.setType(ResponseType.ERROR);
                return response;

            }
        }

    }
}
